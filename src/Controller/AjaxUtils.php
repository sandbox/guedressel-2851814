<?php

namespace Drupal\link_browser_widget\Controller;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\facets\Exception\Exception;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Symfony\Component\HttpFoundation\JsonResponse;

class AjaxUtils {

  public static function getUriAsDisplayableString() {

    if ( ! isset($_POST['uri'])) {
      throw new Exception("Bad Request");
    }
    $uri = filter_input(INPUT_POST, 'uri', FILTER_SANITIZE_STRING);
    if (UrlHelper::isExternal($uri)) {
      $result = $uri;
    } else {
      $result = static::_getUriAsDisplayableString($uri);
    }
    return new JsonResponse($result);
  }

  /**
   * Gets the URI without the 'internal:' or 'entity:' scheme.
   *
   * This is a copy of LinkWidget::getUriAsDisplayableString() !!
   *
   * @see LinkWidget::getUserEnteredStringAsUri()
   */
  protected static function _getUriAsDisplayableString($uri) {
    $scheme = parse_url($uri, PHP_URL_SCHEME);

    // By default, the displayable string is the URI.
    $displayable_string = $uri;

    // A different displayable string may be chosen in case of the 'internal:'
    // or 'entity:' built-in schemes.
    if ($scheme === 'internal') {
      $uri_reference = explode(':', $uri, 2)[1];

      // @todo '<front>' is valid input for BC reasons, may be removed by
      //   https://www.drupal.org/node/2421941
      $path = parse_url($uri, PHP_URL_PATH);
      if ($path === '/') {
        $uri_reference = '<front>' . substr($uri_reference, 1);
      }

      $displayable_string = $uri_reference;
    }
    elseif ($scheme === 'entity') {
      list($entity_type, $entity_id) = explode('/', substr($uri, 7), 2);
      // Show the 'entity:' URI as the entity autocomplete would.
      $entity_manager = \Drupal::entityManager();
      if ($entity_manager->getDefinition($entity_type, FALSE) && $entity = \Drupal::entityManager()->getStorage($entity_type)->load($entity_id)) {
        $displayable_string = EntityAutocomplete::getEntityLabels(array($entity));
      }
    }

    return $displayable_string;
  }
}

