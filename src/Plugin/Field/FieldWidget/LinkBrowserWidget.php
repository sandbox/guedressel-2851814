<?php

namespace Drupal\link_browser_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_browser\Element\EntityBrowserElement;
use Drupal\facets\Exception\Exception;
use Drupal\link\LinkItemInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity browser link widget.
 *
 * @FieldWidget(
 *   id = "link_browser",
 *   label = @Translation("Link Browser"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkBrowserWidget extends LinkWidget implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * Constructs widget plugin.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }


  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
        'entity_browser' => NULL,
      ) + parent::defaultSettings();
  }



  protected function getEntityFromUri( $uri ) {
    if (parse_url($uri, PHP_URL_SCHEME) !== 'entity') {
      throw new Exception("Bad URI");
    }
    $entity_slug = substr($uri, 7);
    list($entity_type, $entity_id) = explode('/', $entity_slug, 2);
    if (! $this->entityTypeManager->getDefinition($entity_type)) {
      throw new Exception("Failed to extract Entity. Slug: " . $entity_slug);
    }
    return $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
  }



  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $browsers = [];
    /** @var \Drupal\entity_browser\EntityBrowserInterface $browser */
    foreach ($this->entityTypeManager->getStorage('entity_browser')->loadMultiple() as $browser) {
      $browsers[$browser->id()] = $browser->label();
    }

    $element['entity_browser'] = [
      '#title' => $this->t('Entity browser'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('entity_browser'),
      '#options' => $browsers,
    ];

    return $element;
  }

  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $browser_id = $this->getSetting('entity_browser');
    if (empty($browser_id)) {
      $summary[] = $this->t('No Entity Browser selected');
    } else {
      $browser = $this->entityTypeManager->getStorage('entity_browser')->load($browser_id);
      $summary[] = $this->t('Entity Browser: @entity_browser', array('@entity_browser' => $browser->label()));
    }
    return $summary;
  }


  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta,
                              array $element, array &$form,
                              FormStateInterface $form_state) {


    /** @var \Drupal\link\LinkItemInterface $item */
    $item = $items[$delta];
    $link_uri = $item->isEmpty() ? NULL : $link_uri = $item->get('uri')->getString();

    $element += [
      '#attributes' => [
        'class' => ['link-browser-element'],
      ],
    ];
    $element['#attached']['library'][] = 'link_browser_widget/widget';

    $element['uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#placeholder' => $this->getSetting('placeholder_url'),
      '#attributes' => [
        'class' => ['link-target']
      ],
      // The current field value could have been entered by a different user.
      // However, if it is inaccessible to the current user, do not display it
      // to them.
      '#default_value' => (!$item->isEmpty() && (\Drupal::currentUser()->hasPermission('link to any page') || $item->getUrl()->access())) ? static::getUriAsDisplayableString($link_uri) : NULL,
      '#element_validate' => array(array(get_called_class(), 'validateUriElement')),
      '#maxlength' => 2048,
      '#required' => $element['#required'],
    ];

    if ($this->supportsInternalLinks()) {

      $entity = NULL;
      if ($link_uri) {
        try {
          $entity = $this->getEntityFromUri($link_uri);
        } catch( \Exception $e ) {
          // Okay to fail: Link URI seems not to be an internal entity reference.
        }
      }


      /** @var \Drupal\entity_browser\EntityBrowserInterface $browser */
      $browser_id = $this->getSetting('entity_browser');
      $browser = $this->entityTypeManager->getStorage('entity_browser')->load($browser_id);

      // Actually this widget should only support entity browsers with a
      // selection display which supports EntityBrowserElement::SELECTION_MODE_EDIT.
      // But currently the whole story about SELECTION_MODE_EDIT is barely
      // implemented in the entity_browser module.
      // Hence a fallback to the SELECTION_MODE_PREPEND works for now.
      // Some related issues:
      //   * https://www.drupal.org/node/2845941
      //   * https://www.drupal.org/node/2738843

      try {
        $browser->getSelectionDisplay()->checkPreselectionSupport();
        $selection_mode = EntityBrowserElement::SELECTION_MODE_EDIT;
        $default_value = [$entity];
      } catch(\Exception $e) {
        $selection_mode = EntityBrowserElement::SELECTION_MODE_PREPEND;
        $default_value = NULL;
      }

      // Above code seems not to work since SELECTION_MODE_EDIT doesn't
      // honor the specified cardinality in tested entity_browser
      // implementations.
      // Going back to hardcoded workaround mode:
      $selection_mode = EntityBrowserElement::SELECTION_MODE_PREPEND;

      $element['browser_button'] = [
        '#type' => 'entity_browser',
        '#entity_browser' => $this->getSetting('entity_browser'),
        '#cardinality' => 1,
        '#selection_mode' => $selection_mode,
        '#default_value' => $default_value,
        '#entity_browser_validators' => [],
      ];

    }

    $element['title'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#placeholder' => $this->getSetting('placeholder_title'),
      '#default_value' => isset($items[$delta]->title) ? $items[$delta]->title : NULL,
      '#maxlength' => 255,
      '#access' => $this->getFieldSetting('title') != DRUPAL_DISABLED,
    );
    // Post-process the title field to make it conditionally required if URL is
    // non-empty. Omit the validation on the field edit form, since the field
    // settings cannot be saved otherwise.
    if( !$this->isDefaultValueWidget($form_state) && $this->getFieldSetting('title') == DRUPAL_REQUIRED ) {
      $element['#element_validate'][] = array(get_called_class(), 'validateTitleElement');
    }

    // Exposing the attributes array in the widget is left for alternate and more
    // advanced field widgets.
    $element['attributes'] = array(
      '#type' => 'value',
      '#tree' => TRUE,
      '#value' => !empty($items[$delta]->options['attributes']) ? $items[$delta]->options['attributes'] : [],
      '#attributes' => [
        'class' => ['link-field-widget-attributes']
      ],
    );

    // TODO: check following code.
    // If cardinality is 1, ensure a proper label is output for the field.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
      // If the link title is disabled, use the field definition label as the
      // title of the 'uri' element.
      if ($this->getFieldSetting('title') == DRUPAL_DISABLED) {
        $element['uri']['#title'] = $element['#title'];
      }
      // Otherwise wrap everything in a details element.
      else {
        $element += array(
          '#type' => 'fieldset',
        );
      }
    }

    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      if ( ! empty($value['uri'])) {
        $link_str = static::getUserEnteredStringAsUri($value['uri']);
      } else {
        $link_str = "";
      }
      $value['uri'] = $link_str;
      $value += ['options' => []];
    }
    return $values;
  }

  /**
   * Returns if the widget can be used for the link field.
   * This widget is only applicable, if the field allows internal links.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition that should be checked.
   *
   * @return bool
   *   TRUE if the widget can be used, FALSE otherwise.
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $link_type = $field_definition->getSetting('link_type');
    return (bool) ($link_type & LinkItemInterface::LINK_INTERNAL);
  }

}
