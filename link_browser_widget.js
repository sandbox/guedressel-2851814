(function ($, Drupal, drupalSettings) {

  'use strict';

  var onEntityBrowserValueUpdated = function() {
    var $this = $(this);
    var urlInput = $this.parent().find('input.link-target');
    var ebVal = $this.val();
    var entityInfo = ebVal.split(':', 2);
    var linkVal = 'entity:' + entityInfo[0] + "/" + entityInfo[1];
    urlInput.val(Drupal.t('Loading...'));
    var ajaxObj = urlInput.data('link-browser-ajax-object');
    ajaxObj.options.data.uri = linkVal;
    ajaxObj.execute();
  };

  Drupal.behaviors.linkBrowserWidget = {

    'attach': function (context) {

      var $widgets = $('.field--widget-link-browser', context);
      $widgets.find('input.eb-target:not(.link-browser-processed)')
        .on('entity_browser_value_updated', onEntityBrowserValueUpdated)
        .addClass('link-browser-processed')
        .each(function() {
          var $this = $(this);
          var urlInput = $this.parent().find('input.link-target');
          var ajaxOptions = {
            url: drupalSettings.path.baseUrl + "admin/link-browser/uri-as-displayable-string",
            keypress: false

          };
          var urlAjaxObj = new Drupal.Ajax(urlInput.attr('id'), urlInput[0], ajaxOptions);
          urlAjaxObj.options.success = function(response, status, xmlhttprequest) {
            urlInput.val(response);
            urlAjaxObj.success();
          };
          urlInput.data('link-browser-ajax-object', urlAjaxObj);
        });
    }
  };

})(jQuery, Drupal, drupalSettings);
