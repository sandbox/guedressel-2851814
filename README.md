#Link Browser Widget

Link Browser Widget is an extended variant of the default widget for Link fields.
It embeds an entity browser to make lookup of link targets to site internal entities more convenient.

By it's nature of adding UX bless for creating internal links it is only applicable on link fields where link type "internal" is allowed.

