#Development Drupal Site

This (composer based) Drupal site is purposed to ease
development of the link_browser_widget module.
It bootstraps a fully configured Drupal site to
work on the module.

## Initialize

```
#> cd link_browser_widget/.machinery
#> composer install
#> composer drupal-scaffold
#> ./vendor/bin/drupal site:install \
#    --site-name "Link Browser" \
#    link_browser_widget_profile
```